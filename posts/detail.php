<? require($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Пост</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
			<? includeComponent('post_detail', 'post_detail', [
				'post_id' => $_GET['id']
			]) ?>
			<? 
			 $id_GET = intval();
			 $query = "SELECT p.*, u.firstname AS author_firstname, u.lastname AS author_lastname
						FROM posts p, users u
						WHERE p.author_id = u.id AND p.id=$id_GET
						ORDER BY p.date DESC";
			$res = $mysql->query($query);
			$post = $res->fetch_assoc();

			if ($post) { ?>
				<? $post['content'] ?>
				<article>
					<h2><?= $post['title'] ?></h2>
					<img src="<?= $post['preview_img'] ?>" alt="">
					<p style="white-space: pre-wrap;"><?= $post['content'] ?></p>
					<h3>Автор: <?= $post['author_firstname'] ?> <?= $post['author_lastname'] ?></h3>
					<p>Дата публикации: <?= $post['date'] ?></p>
				</article>
				<a href="/posts/">Назад</a>
			<? } else { ?>
				 Пост с указанным id не найден
			<? } ?>
           </div>
        </div>
    </div>
<? require($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>