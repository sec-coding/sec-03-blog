<? require($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>

<?
$firstname = trim($_POST['firstname']);
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$nickname = $_POST['nickname'];
$password = $_POST['password'];
$repeat = $_POST['repeat'];
$photo = $_FILES['photo'];

if ($email && $password && $firstname && $lastname && $photo) {
	
    //Если основные поля заполнены то идем дальше
    
    if ($repeat === $password) {
        
        //Если пароли совпали то идем дальше

        $salt = '123qwerty999*****---';
        $hash = md5($salt . $password);

        //Проверяем есть ли в базе такой пользователь

        $count_query = "SELECT COUNT(*) AS count
                        FROM users
                        WHERE login = '$email'";
        $count_res = $mysql->query($count_query);
        $count_arr = $count_res->fetch_assoc();
        $user_count = intval($count_arr['count']);
 
        if($user_count == 0) {
 
            //Добавляем запись в базу

            if (!copy($photo['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/upload/avatar-$email.jpg")) {
                die("Не удалось скопировать файл");
            }

            $insert = "INSERT INTO users (firstname, lastname, nickname, login, password, avatar)" .
            "VALUES('{$firstname}', '{$lastname}', '{$nickname}', '{$email}', '{$hash}', '/upload/avatar-$email.jpg')";
            $ins = $mysql->query($insert);

            if ($ins) { 
                ?>
                все ок. вы будете перенаправлены на главную старницу.
                <script>
                    //setTimeout(function(){
                    //    window.location = '/';
                    //}, 2000)
                </script>
            <? } else {
            };

        } else { echo "Пользователь с таким именем существует! "; }
        
    } else {
        echo 'Пароли не совпадают';
    };
};

?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Регистрация</h1>
                <form method="post" enctype="multipart/form-data">
                    <label>Имя</label>
                    <input type='text' name='firstname' placeholder='Имя'>
                    <br>
                    <label>Фамилия</label>
                    <input type='text' name='lastname' placeholder='Фамилия' >
                    <br>
                    <label>Никнэйм</label>
                    <input type='text' name='nickname' placeholder='Никнэйм'>
                    <br>
                    <p>Загрузите вашу фотографию</p>
                    <input type="file" name="photo" accept="image/jpeg" required>
                    <label>Эл. почта</label>
                    <input type='email' name='email' placeholder='Эл. почта'>
                    <br>
                    <label>Пароль</label>
                    <input type='password' name='password' placeholder='Пароль'>
                    <br>
                    <label>Повторите</label>
                    <input type='password' name='repeat' placeholder='Повторите' >
                    <br>

                    <button>Зарегистрироваться</button>
                
                </form>
            </div>
        </div>
    </div>
<? require($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>
