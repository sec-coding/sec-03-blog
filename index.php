<? require($_SERVER['DOCUMENT_ROOT'] . '/framework/site_template/header.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Главная</h1>

	            <? includeComponent('post_detail', 'post_detail', [
					'post_id' => 55
				]) ?>

	            <? includeComponent('post_list', 'main', [
					'posts_count' => 3,
					'need_paginator' => false
				]) ?>
            </div>
        </div>
    </div>


<? require($_SERVER['DOCUMENT_ROOT'] . '/framework/site_template/footer.php'); ?>