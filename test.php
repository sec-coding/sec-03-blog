<? 
$hasData = false;
if (isset($_GET['day']) && isset($_GET['month']) && isset($_GET['year'])) {
    $hasData = true;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<? if ($hasData) {

    # проверка данных
    $error = false;

    $day = intval($_GET['day']);
    $month = intval($_GET['month']);
    $year = intval($_GET['year']);
    if ($day < 1 || $day > 31) {
        $error = true;
        echo 'Некорректный день!<br>';
    }
    if ($month < 0 || $month > 11) {
        $error = true;
        echo 'Некорректный месяц!<br>';
    }
    if ($year < 1950 || $year > 2017) {
        $error = true;
        echo 'Некорректный год!<br>';
    }
    ?>

    <? if ($error) { ?>
        К сожалению, произошла ошибка. Перейдите <a href="/">на главную страницу</a> и попробуйте ещё раз. 
    <? } else {
        # полезное действие с данными ?>
        Спасибо за ввод данных!
    <? } ?>

<? } else { ?>

    <form method="get">
        <label>
            day
            <select name="day" id="">
                <? for($i = 1; $i <= 31; $i++) { ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                <? } ?>
            </select>
        </label>
        <label>
            month
            <?
                $months = [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                ];
            ?>
            <select name="month" id="">
                <? foreach($months as $i => $month) { ?>
                    <option value="<?= $i ?>"><?= $month ?></option>
                <? } ?>
            </select>
        </label>
        <label>
            year
            <select name="year" id="">
                <? for($i = 2017; $i > 1949; $i--) { ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                <? } ?>
            </select>
        </label>
        <button type="submit">отправить!</button>
    </form>

<? } ?>






<pre>
<?
# var_dump($_GET);
?>
</pre>

<pre>
<?
# var_dump($month);
?>
</pre>

<pre>
<?
# print_r($month);
?>
</pre>
</body>
</html>