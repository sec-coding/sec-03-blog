<?
require($_SERVER['DOCUMENT_ROOT'] . '/framework/model.php');
session_start();
$mysql = new mysqli("localhost", "root", "", "blog");
if ($mysql->connect_errno) {
    die("Не удалось подключиться к MySQL: " . $mysql->connect_error);
}
$_SESSION['mysql'] = $mysql;

function includeComponent($componentName, $templateName, $arParams) {
	require($_SERVER['DOCUMENT_ROOT'] . "/framework/components/$componentName.php");
}