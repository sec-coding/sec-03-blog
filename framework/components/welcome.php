<?
if ($_GET['logout'] === 'Y') {
    unset($_SESSION['user_id']);
} else if ($_SESSION['user_id']) {
	$arResult = [
		'user' => getUserData($_SESSION['user_id'])
	];
	require($_SERVER['DOCUMENT_ROOT'] . "/framework/templates/$templateName.php"); 
}